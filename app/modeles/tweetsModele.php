<?php
/*
  ./app/modeles/ajax.php
 */
namespace App\Modeles\TweetsModele;


 function findOneRandom(\PDO $connexion){
   $sql = "SELECT *
           FROM tweets
           JOIN users ON tweets.userID = users.id
           ORDER BY RAND()
           LIMIT 1;";

  $rs = $connexion->query($sql);
  return $rs->fetch(\PDO::FETCH_ASSOC);
 }
