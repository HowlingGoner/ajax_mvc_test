<?php
/*
    ./app/controleurs/defaultsControleur.php
 */
namespace App\Controleurs\TweetsControleur;
use App\Modeles\TweetsModele as TweetsModele;

function randomAction($connexion){
  // Va chercher les infos dans la DB
  include_once '../app/modeles/tweetsModele.php';
  $tweet = TweetsModele\findOneRandom($connexion);
  // Charge une vue
  include '../app/vues/tweets/random.php';
}
