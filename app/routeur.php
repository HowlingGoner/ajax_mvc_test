<?php
/*
  ./app/routeur.php
 */

if(isset($_GET['ajax'])) :
  include_once '../app/controleurs/testAjaxControleur.php';
    App\Controleurs\testAjaxControleur\coucouAction();

  /*
  PATTERN : /?ajax-tweets-random
  CTRL : tweetsControleur
  ACTION : randomAction
   */
  elseif(isset($_GET['ajax-tweets-random'])) :
    include_once '../app/controleurs/tweetsControleur.php';
    App\Controleurs\TweetsControleur\randomAction($connexion);
endif;
