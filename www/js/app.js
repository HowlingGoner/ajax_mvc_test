/*
  ./www/js/app.js
 */

$(function(){
  $('#hop').click(function(){
    $.ajax({
      url: 'ajax'
    })
    .done(function(reponsePHP){
      $('#hip').html(reponsePHP);
    })
    .fail(function(){
      alert("Problème durant la transaction !");
    });
  })

  $('#monBouton').click(function(){
    $.ajax({
      url: 'ajax-tweets-random'
    })
    .done(function(reponsePHP){
      $('#monDiv').html(reponsePHP).hide().slideDown();
    })
    .fail(function(){
      alert("Problème durant la transaction !");
    });
  })
});
